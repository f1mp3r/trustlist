var gulp = require('gulp');
var minifyCSS = require('gulp-csso');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var minify = require('gulp-minify');

gulp.task('css', function(){
	return gulp.src('assets/css/*.css')
		.pipe(minifyCSS())
		.pipe(concat('app.min.css'))
		.pipe(gulp.dest('web/build/css'));
});

gulp.task('js', function(){
	return gulp.src([
			'assets/js/jquery.min.js',
			'assets/js/popper.min.js',
			'assets/js/bootstrap.min.js',
			'assets/js/autoresize.js',
			'assets/js/script.js'
		])
		.pipe(sourcemaps.init())
		.pipe(concat('app.js'))
		.pipe(sourcemaps.write())
		.pipe(minify())
		.pipe(gulp.dest('web/build/js'))
});

gulp.task('fonts', function(){
	return gulp.src('assets/fonts/*')
		.pipe(gulp.dest('web/build/fonts'));
});

gulp.task('watch', function() {
	gulp.watch('assets/js/script.js', ['js'])
	gulp.watch('assets/css/*.css', ['css'])
});

gulp.task('default', ['css', 'js', 'fonts']);