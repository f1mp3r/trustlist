-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 03, 2018 at 08:37 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trustlist`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `slug` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`) VALUES
(1, 'News', 'news'),
(2, 'Media', 'media'),
(3, 'Blogs', 'blogs');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(11) NOT NULL,
  `website_id` int(11) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `agent` varchar(300) NOT NULL,
  `rating` tinyint(4) NOT NULL,
  `date_created` datetime NOT NULL,
  `type` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `websites`
--

CREATE TABLE `websites` (
  `id` int(11) NOT NULL,
  `domain` varchar(200) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  `rss_url` varchar(200) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `avg_rating` tinyint(4) NOT NULL DEFAULT '0',
  `avg_trust_rating` tinyint(4) NOT NULL DEFAULT '0',
  `avg_clickbait_rating` tinyint(4) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT 0,
  `adder_ip` varchar(100) NOT NULL DEFAULT '',
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `websites`
--

INSERT INTO `websites` (`id`, `domain`, `description`, `rss_url`, `category_id`, `avg_rating`, `avg_trust_rating`, `avg_clickbait_rating`, `clicks`, `adder_ip`, `date_created`) VALUES
(1, 'bbc.co.uk', NULL, NULL, 1, 0, 0, 0, 0, '', '0000-00-00 00:00:00'),
(2, 'news.google.com', NULL, NULL, 1, 0, 0, 0, 0, '', '0000-00-00 00:00:00'),
(3, 'bbc.com', NULL, NULL, 1, 0, 0, 0, 0, '', '0000-00-00 00:00:00'),
(4, 'msn.com', NULL, NULL, 1, 0, 0, 0, 0, '', '0000-00-00 00:00:00'),
(5, 'express.co.uk', NULL, NULL, 1, 0, 0, 0, 0, '', '0000-00-00 00:00:00'),
(6, 'thesun.co.uk', NULL, NULL, 1, 0, 0, 0, 0, '', '0000-00-00 00:00:00'),
(7, 'metro.co.uk', NULL, NULL, 1, 0, 0, 0, 0, '', '0000-00-00 00:00:00'),
(8, 'mirror.co.uk', NULL, NULL, 1, 0, 0, 0, 0, '', '0000-00-00 00:00:00'),
(9, 'theguardian.com', NULL, NULL, 1, 0, 0, 0, 0, '', '0000-00-00 00:00:00'),
(10, 'cnet.com', NULL, NULL, 1, 0, 0, 0, 0, '', '0000-00-00 00:00:00'),
(11, 'ndtv.com', NULL, NULL, 1, 0, 0, 0, 0, '', '0000-00-00 00:00:00'),
(12, 'facebook.com', NULL, NULL, 2, 0, 0, 0, 0, '', '0000-00-00 00:00:00'),
(13, '6nine.net', '', NULL, 3, 0, 0, 0, 0, '', '2018-01-09 01:15:09'),
(14, 'yahoo.com', '', NULL, 1, 0, 0, 0, 0, '', '2018-01-24 09:53:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);
ALTER TABLE ratings MODIFY id int AUTO_INCREMENT PRIMARY KEY;

--
-- Indexes for table `websites`
--
ALTER TABLE `websites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_domain` (`domain`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `websites`
--
ALTER TABLE `websites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
