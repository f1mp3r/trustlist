<?php


use Phinx\Migration\AbstractMigration;

class ChangeWebsiteNameField extends AbstractMigration
{
    public function change()
    {
	    $this
		    ->table('websites')
		    ->changeColumn(
		    	'name',
			    'string',
			    [
				    'limit' => 40,
				    'after' => 'description',
				    'null' => true,
				    'default' => null
			    ]
		    )
		    ->save();
    }
}
