<?php


use Phinx\Migration\AbstractMigration;

class CreateCommentsTable extends AbstractMigration
{
    public function change()
    {
	    $this
		    ->table('comments')
		    ->addColumn('website_id', 'integer')
		    ->addColumn('rating_id', 'integer', ['null' => true, 'default' => null])
		    ->addColumn('comment', 'text')
		    ->addColumn('ip', 'string', ['limit' => 32])
		    ->addColumn('country_code', 'string', ['limit' => 2, 'null' => true])
		    ->addColumn('region', 'string', ['limit' => 30, 'null' => true])
		    ->addColumn('date_added', 'datetime')
		    ->addIndex(['website_id'])
		    ->addIndex(['rating_id'])
		    ->addIndex(['ip'])
		    ->save()
	    ;

	    $this
		    ->table('ratings')
		    ->addColumn('country_code', 'string', ['limit' => 30, 'null' => true])
		    ->addColumn('region', 'string', ['limit' => 30, 'null' => true])
		    ->addIndex(['website_id', 'type', 'ip'])
		    ->save()
	    ;

	    if ($this->isMigratingUp()) {
		    $this
			    ->table('ratings')
			    ->changeColumn('ip', 'string', ['limit' => 35])
			    ->save();
	    }
    }
}
