<?php


use Phinx\Migration\AbstractMigration;

class CreateFeedbackTable extends AbstractMigration
{
    public function change()
    {
	    $this
		    ->table('feedback')
		    ->addColumn('name', 'string', ['limit' => 50])
		    ->addColumn('email', 'string', ['limit' => 100])
		    ->addColumn('text', 'text')
		    ->addColumn('ip', 'string', ['limit' => 64])
		    ->addColumn('date_posted', 'datetime')
		    ->save()
	    ;
    }
}
