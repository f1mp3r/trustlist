<?php


use Phinx\Migration\AbstractMigration;

class AddFaviconFieldToWebsites extends AbstractMigration
{
    public function change()
    {
	    $this
		    ->table('websites')
		    ->addColumn(
		    	'favicon',
			    'string',
			    [
			    	'null' => true,
				    'default' => null,
				    'limit' => 60,
				    'after' => 'description'
			    ]
		    )
		    ->addColumn(
		    	'snapshot',
			    'string',
			    [
				    'null' => true,
				    'default' => null,
				    'limit' => 60,
				    'after' => 'description'
			    ]
		    )
		    ->save()
	    ;
    }
}
