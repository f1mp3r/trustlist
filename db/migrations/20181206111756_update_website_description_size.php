<?php


use Phinx\Migration\AbstractMigration;

class UpdateWebsiteDescriptionSize extends AbstractMigration
{
    public function change()
    {
	    $this
		    ->table('websites')
		    ->changeColumn('description', 'string', ['limit' => 500])
		    ->save();
    }
}
