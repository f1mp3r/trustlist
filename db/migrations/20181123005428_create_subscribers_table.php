<?php


use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

class CreateSubscribersTable extends AbstractMigration
{
    public function change()
    {
	    $this
		    ->table('subscribers')
		    ->addColumn('frequency', 'integer', ['length' => MysqlAdapter::INT_TINY])
		    ->addColumn('category', 'integer', ['length' => MysqlAdapter::INT_TINY, 'null' => true])
		    ->addColumn('name', 'string', ['null' => true])
		    ->addColumn('email', 'string')
		    ->addColumn('last_email_sent_date', 'datetime', ['null' => true])
		    ->addColumn('verified', 'boolean', ['default' => false])
		    ->addColumn('verification_hash', 'string', ['length' => 64])
		    ->addColumn('date_added', 'datetime')
		    ->save()
	    ;
    }
}
