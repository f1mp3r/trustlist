<?php


use Phinx\Migration\AbstractMigration;

class AddNameToWebsites extends AbstractMigration
{
    public function change()
    {
	    $this
		    ->table('websites')
		    ->addColumn(
		    	'name',
			    'string',
			    [
			    	'limit' => 15,
				    'after' => 'description',
				    'null' => true,
				    'default' => null
			    ]
		    )
		    ->save()
	    ;
    }
}
