<?php


use Phinx\Migration\AbstractMigration;

class AddParentToComments extends AbstractMigration
{
    public function change()
    {
	    $this
		    ->table('comments')
		    ->addColumn('parent_id', 'integer', ['null' => true, 'default' => null])
		    ->save()
	    ;
    }
}
