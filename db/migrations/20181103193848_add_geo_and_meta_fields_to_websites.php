<?php


use Phinx\Migration\AbstractMigration;

class AddGeoAndMetaFieldsToWebsites extends AbstractMigration
{
    public function change()
    {
    	$this
		    ->table('ratings')
		    ->addIndex(['website_id'])
		    ->addIndex(['ip'])
		    ->save()
	    ;

	    $this
		    ->table('websites')
		    ->addColumn('geo_id', 'integer', ['null' => true, 'default' => null, 'after' => 'favicon'])
		    ->addColumn('meta_description', 'string', ['null' => true, 'default' => null, 'after' => 'favicon'])
		    ->addColumn('meta_keywords', 'string', ['null' => true, 'default' => null, 'after' => 'favicon'])
		    ->addColumn('add_requests_count', 'integer', ['default' => 0, 'after' => 'favicon'])
		    ->save()
	    ;

	    $this
		    ->table('geos')
		    ->addColumn('name', 'string')
		    ->save()
	    ;

	    if ($this->isMigratingUp()) {
	    	$this
			    ->table('geos')
			    ->insert([
				    ['name' => 'Global'],
				    ['name' => 'US'],
				    ['name' => 'UK'],
				    ['name' => 'India'],
			    	['name' => 'Germany'],
			    	['name' => 'Russia'],
			    	['name' => 'Ukraine'],
			    	['name' => 'China'],
			    	['name' => 'Canada'],
			    	['name' => 'Spain'],
			    	['name' => 'Bulgaria'],
			    ])
			    ->saveData()
		    ;
	    } else {
	    	$this->table('geos')->truncate();
	    }
    }
}
