<?php

use Faker\Factory;
use Phinx\Seed\AbstractSeed;

class CommentsSeeder extends AbstractSeed
{
	public function run()
	{
		$faker = Factory::create();
		$websiteIds = array_column($this->fetchAll('SELECT id FROM websites WHERE approved = 1'), 'id');
		$data = [];

		for ($i = 0; $i < 500; $i++) {
			$data[] = [
				'website_id'    => $faker->randomElement($websiteIds),
				'comment'       => $faker->text($faker->numberBetween(15, 1500)),
				'ip'            => md5($faker->ipv4),
				'country_code'  => strtolower($faker->countryCode),
				'region'        => $faker->city,
				'date_added'    => $faker->dateTime()->format('Y-m-d H:i:s'),
			];
		}

		$this
			->table('comments')
			->insert($data)
			->saveData();
	}
}
