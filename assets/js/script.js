jQuery.fn.outerHTML = function () {
	return jQuery('<div />').append(this.eq(0).clone()).html();
};

function isBot(){
	var botPattern = "(googlebot\/|Googlebot-Mobile|Googlebot-Image|Google favicon|Mediapartners-Google|bingbot|slurp|java|wget|curl|Commons-HttpClient|Python-urllib|libwww|httpunit|nutch|phpcrawl|msnbot|jyxobot|FAST-WebCrawler|FAST Enterprise Crawler|biglotron|teoma|convera|seekbot|gigablast|exabot|ngbot|ia_archiver|GingerCrawler|webmon |httrack|webcrawler|grub.org|UsineNouvelleCrawler|antibot|netresearchserver|speedy|fluffy|bibnum.bnf|findlink|msrbot|panscient|yacybot|AISearchBot|IOI|ips-agent|tagoobot|MJ12bot|dotbot|woriobot|yanga|buzzbot|mlbot|yandexbot|purebot|Linguee Bot|Voyager|CyberPatrol|voilabot|baiduspider|citeseerxbot|spbot|twengabot|postrank|turnitinbot|scribdbot|page2rss|sitebot|linkdex|Adidxbot|blekkobot|ezooms|dotbot|Mail.RU_Bot|discobot|heritrix|findthatfile|europarchive.org|NerdByNature.Bot|sistrix crawler|ahrefsbot|Aboundex|domaincrawler|wbsearchbot|summify|ccbot|edisterbot|seznambot|ec2linkfinder|gslfbot|aihitbot|intelium_bot|facebookexternalhit|yeti|RetrevoPageAnalyzer|lb-spider|sogou|lssbot|careerbot|wotbox|wocbot|ichiro|DuckDuckBot|lssrocketcrawler|drupact|webcompanycrawler|acoonbot|openindexspider|gnam gnam spider|web-archive-net.com.bot|backlinkcrawler|coccoc|integromedb|content crawler spider|toplistbot|seokicks-robot|it2media-domain-crawler|ip-web-crawler.com|siteexplorer.info|elisabot|proximic|changedetection|blexbot|arabot|WeSEE:Search|niki-bot|CrystalSemanticsBot|rogerbot|360Spider|psbot|InterfaxScanBot|Lipperhey SEO Service|CC Metadata Scaper|g00g1e.net|GrapeshotCrawler|urlappendbot|brainobot|fr-crawler|binlar|SimpleCrawler|Livelapbot|Twitterbot|cXensebot|smtbot|bnf.fr_bot|A6-Indexer|ADmantX|Facebot|Twitterbot|OrangeBot|memorybot|AdvBot|MegaIndex|SemanticScholarBot|ltx71|nerdybot|xovibot|BUbiNG|Qwantify|archive.org_bot|Applebot|TweetmemeBot|crawler4j|findxbot|SemrushBot|yoozBot|lipperhey|y!j-asr|Domain Re-Animator Bot|AddThis)";
	var re = new RegExp(botPattern, 'i');
	var userAgent = navigator.userAgent;

	return re.test(userAgent);
}

var isMobile = function () {
	var check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
	return check;
};

var getDomain = function (url) {
	var split1 = url.split('//')[1];
	var split2 = split1.split('/')[0];

	var domain = split2;
	if (split2.substring(0, 4) == 'www.') {
		domain = split2.slice(4);
	}

	return domain;
};

var themes = {
	dark: {
		class: 'dark',
		iconClasses: 'fa fa-moon-o text-dark'
	},
	light: {
		class: 'light',
		iconClasses: 'fa fa-sun-o text-warning'
	}
};

var adjustBackgroundDivs = function () {
	setTimeout(function () {
		$('.card.website').each(function() {
			$('.background', $(this)).css('height', $(this).height() + 'px');
		});
	}, 80);
};

var themeManager = {
	init: function () {
		var theme = themes['light'];

		if (localStorage.getItem('theme')) {
			theme = themes[localStorage.getItem('theme')];
		} else {
			localStorage.setItem('theme', 'light');
		}

		themeManager.setTheme(theme);
	},
	setTheme: function (theme) {
		var isLight = 'light' == theme.class;

		$('body').removeClass(isLight ? 'dark' : 'light').addClass(theme.class);

		themeManager.updateThemeClasses(isLight);
		themeManager.updateCardsBackgrounds();

		$('.theme-switch i')
			.removeClass()
			.addClass('dark' == theme.class ? themes.light.iconClasses : themes.dark.iconClasses);
	},
	updateThemeClasses: function (isLight) {
		$('[data-light], [data-dark]').each(function (k, obj) {
			obj = $(obj);

			if (isLight && obj.data('dark')) {
				obj.removeClass(obj.data('dark'));

				if (obj.data('light')) {
					obj.addClass(obj.data('light'));
				}
			} else if (!isLight && obj.data('light')) {
				obj.removeClass(obj.data('light'));

				if (obj.data('dark')) {
					obj.addClass(obj.data('dark'));
				}
			}
		});
	},
	currentThemeIsLight: function () {
		var currentTheme = $('body').attr('class');
		return 'light' == currentTheme;
	},
	getCorrectCardBackground: function (snapshot) {
		var style = $('body').hasClass('light') ? backgroundCss.light : backgroundCss.dark;

		return style.replace("%s", snapshot);
	},
	updateCardsBackgrounds: function () {
		$('.card.website > .background').each(function () {
			if ($(this).data('background')) {
				$(this).css('background', themeManager.getCorrectCardBackground($(this).data('background')));
				$(this).css('background-size', 'cover');
				$(this).css('background-position', 'center');
				$(this).css('transition', 'all 0.5s ease');
			}
		});
		adjustBackgroundDivs();
	}
};

var objectToQuerystring = function (params) {
	var newParams = {};
	$.each(params, function (k, v) {
		if (v != null) {
			newParams[k] = v;
		}
	});

	var result = Object.keys(newParams).map(function(key) {
		return key + '=' + newParams[key]
	}).join('&');

	return result;
}

var backgroundCss = {
	light: 'linear-gradient(rgba(202, 220, 239, 0.9), rgba(202, 220, 239, 0.9)), url("%s") no-repeat center center',
	dark: 'linear-gradient(rgba(39, 64, 89, 0.9), rgba(39, 64, 89, 0.9)), url("%s") no-repeat center center'
};

themeManager.init();

$(function () {
	$('.theme-switch').click(function() {
		var currentTheme = $('body').attr('class');
		var theme = 'light' == currentTheme ? 'dark' : 'light';
		localStorage.setItem('theme', theme);

		themeManager.setTheme(themes[theme]);
	});

	$('#feedback > a').click(function () {
		$(this).parent().toggleClass('closed');
	});

	$(window).on('resize', function () {
		var width = $(document).width();

		if (768 > width || isMobile()) {
			$('#subscribe-icon, #subscribe-bar').addClass('d-none');
			$('#feedback').hide();
		} else {
			$('#subscribe-icon, #subscribe-bar').removeClass('d-none');
			$('#feedback').show();
		}
	}).resize();

	$('#subscribe-bar .hidebar').click(function () {
		pageOps.hideSubscribeBar(false);
	});

	$('#subscribe-icon').click(function () {
		$('#subscribe-bar').fadeIn(100);
		$(this).fadeOut(150);
		localStorage.removeItem('subscribeHidden');
	});

	if ($(document).width() > 768) {
		if (!localStorage.getItem('subscribeHidden')) {
			$('#subscribe-bar').fadeIn(100);
		}
		else if (localStorage.getItem('subscribeHidden') == '1') {
			$('#subscribe-icon').fadeIn(100);
		}
	}

	$('#newform').on('shown.bs.modal', function (e) {
		$('#add-domain').focus();
	})

    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    });

    // handle clicking website
    $(document).on('click', '.outlink', function (e) {
        var id = $(this).data('id') ? $(this).data('id') : $(this).parents('.website').data('id');
        $.post('/log/' + id);
    });

	$(document).on('click', '.website-details-open', function (e) {
		// Bots should always load full page
	    if (isBot()) {
	    	return true;
	    }

		e.preventDefault();
		loader.show();
		var domain = $(this).parents('.website').data('domain');
		pageOps.dim();
		pageOps.getWebsiteDetails(domain);
		loader.hide();
	});

	$('.overlay').click(function () {
	    pageOps.undim();
    });

	$('body').keyup(function(e) {
		if ('Escape' === e.key) {
			pageOps.closeWebsiteDetails();
		}
	});

	var loader = $('#page-loading');

	var siteHash = null;

	var pageOps = {
	    dim: function (dim) {
	        dim = undefined === dim ? true : dim;

	        if (!dim) {
	            pageOps.undim();
	            return true;
            }

		    $('body').addClass('unscrollable');
		    $('.overlay').css({width: $(document).width() + 'px', height: $(document).height() + 'px'}).fadeIn(100);
        },
        undim: function () {
	        $('.overlay').fadeOut(100);
	        $('body').removeClass('unscrollable');
        },
        changePageState: function changePageState(data, urlPath){
	    	if (data.title) {
			    document.title = data.title;
		    }

	    	var sendData = {};
	    	if (data.html) {
	    		sendData.html = data.html;
		    }
	    	if (data.title) {
	    		sendData.pageTitle = data.title;
		    }
	    	if (data.elements) {
	    		sendData.elements = data.elements;
		    }

	        window.history.pushState(sendData, "", urlPath);
        },
		openWebsiteDetailsPopup: function (websiteData) {
            siteHash = pageOps.generateRandomString();

            var container = $('<div></div>');
            container.attr('id', siteHash);
            container.data('id', websiteData.id);
            container.addClass('website-details');
            container.html(websiteData.html);

            container.appendTo($('body'));
            container = $('#' + siteHash);

			container.css(pageOps.getContainerSizes($('#' + siteHash)));

			$('.data', container).css({
				height: (container.outerHeight() - container.find('.title:first').outerHeight()) + 'px',
				overflow: 'auto',
				overflowX: 'hidden'
			});

			$('.overlay, .close-details').click(function () {
				pageOps.closeWebsiteDetails();
            });

			$('.new-comment').each(function () {
				$(this).autoResize({
					resizeAlso: $('.submit-comment', $(this).parents('.new-comment-form'))
				});
			});

			pageOps.registerOnResize($('#' + siteHash));
			pageOps.dim();
        },
        getWebsiteDetails: function (domain) {
	        $.ajax({
		        method: 'GET',
		        url: '/site/' + domain,
		        success: function (data) {
                    pageOps.changePageState({
                        title: data.title,
	                    container: 'body',
	                    html: $('body').html()
                    }, '/site/' + domain)

			        pageOps.openWebsiteDetailsPopup(data);
		        },
		        error: function (err) {
			        console.error(err);
		        },
	        });
        },
		closeWebsiteDetails: function () {
			pageOps.undim();
			pageOps.changePageState({title: "All websites"}, '/');
			$('#' + siteHash).remove();
		},
		generateRandomString: function () {
            var text = 's';
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 5; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        },
		registerOnResize: function (element) {
			$(window).resize(function () {
				if (element.length == 0) {
					return;
				}

				element.css(pageOps.getContainerSizes(element));
				//todo: make mask resize as well

				$('.data', element).css({
					height: (element.outerHeight() - element.find('.title:first').outerHeight()) + 'px',
					overflow: 'auto',
					overflowX: 'hidden'
				});
			});
		},
		hideSubscribeBar: function (subscribed) {
			var bar = $('#subscribe-bar');

			bar.fadeOut(150);
			localStorage.setItem('subscribeHidden', (subscribed ? '2' : '1'));

			if (!subscribed) {
				$('#subscribe-icon').show();
			}
		},
		getNewUrl: function (page, query) {
	    	var queryString = query;
	    	if (query instanceof Object && query.constructor === Object) {
			    queryString = objectToQuerystring(query);
		    }

			return (!page ? [location.protocol, '//', location.host, location.pathname].join('') : page) + '?' + queryString;
		},
		getContainerSizes: function (element) {
	    	var normal = {
			    height: (window.innerHeight * 0.9) + 'px',
			    top: (window.innerHeight * .05) + 'px',
			    left: ($(document).width() / 2 - element.width() / 2) + 'px'
		    };
            element.css('max-width', '90%');

	    	if (isMobile()) {
	    		console.log('Set width to ' + window.innerWidth);
	    		element.css('max-width', '100%');
	    		return {
	    			height: window.innerHeight + 'px',
			        top: 0 + 'px',
				    left: 0 + 'px',
				    width: window.innerWidth + 'px'
			    };
		    }

	    	return normal;
		}
    };

    var query = {};
    var globalTitle = {};
    var generateTitle = function (titles) {
    	var generated = [];

    	if (titles.search) {
    		generated.push('Searching for ' + titles.search);
	    }

    	if (titles.category) {
    		generated.push((titles.search ? 'in ' : 'Category ') + titles.category);
	    }

    	if (titles.geo) {
    		generated.push((generated.length ? ' in ' : 'Region ') + titles.geo);
	    }

    	if (titles.sort && generated.length) {
    		generated.push('sorted by ' + titles.sort);
	    }

    	// Todo: add case for only geo and sort

	    if (!generated.length) {
	    	generated.push($('body').data('title'));
	    }

	    var complete = generated.join(' ');
	    return complete.charAt(0).toUpperCase() + complete.slice(1) + ' - ' + $('body').data('project');
    }

	jQuery.extend(query, {
		search: '',
		categoryId: 0,
		sort: 0,
		page: 0,
		geoId: 0
	}, $('#websites').data('query'));

	if (query.search) {
		$('#search').val(query.search);
		globalTitle.search = query.search;
	}

	if (query.categoryId) {
		globalTitle.category = $('#category-menu-item-' + query.categoryId).text().toLowerCase();
	}

	if (query.geoId) {
		$('#region-select').val(query.geoId);
	}

	if (query.sort) {
		$('#sort').val(query.sort);
	}

	if (globalTitle) {
		if (query.geoId) {
			globalTitle.geo = $('#region-select option:selected').text();
		}

		if (query.sort) {
			globalTitle.sort = $('#sort option:selected').text().toLowerCase();
		}
	}

	if (globalTitle) {
		$('title').text(generateTitle(globalTitle));
	}

	window.onpopstate = function(e){
		if(e.state){
			if (e.state.html) {
				$(e.state.selector ? e.state.container : "#websites").html(e.state.html);
			}

			if (e.state.elements) {
				$.each(e.state.elements, function (selector, data) {
					switch (data.type) {
						case 'input':
							$(selector).val(data.value);
							break;
						default:
						case 'div':
							$(selector).html(data.value);
							break;
					}
				});
			}

			document.title = e.state.pageTitle;
		}
	}

	var hasReachedLastResult = false;

    // region Handle category change
    $('#categoryselect a').click(function () {
        var categoryId = $(this).data('id');

        if (categoryId != parseInt(categoryId)) {
            return ;
        }

        if (query.categoryId == categoryId) {
            return ;
        }

        query.categoryId = categoryId;
        query.page = 0;
        hasReachedLastResult = false;

        if (!categoryId) {
        	delete globalTitle.category;
        }

        $('#websites').data('query', query);

        $('#categoryselect a.active').removeClass('active');

        $(this).addClass('active');

        fetchWebsites(query, false, function () {
            var currentCategory = $('#categoryselect a.active');

            if (categoryId) {
	            globalTitle.category = currentCategory.data('title');
            }

	        pageOps.changePageState({
		        title: generateTitle(globalTitle),
                html: $('#websites').html()
	        },
            (currentCategory.data('slug') ? pageOps.getNewUrl(currentCategory.data('slug'), query) : ''))
        });
    });
    // endregion

    var ratingsConfig = {
        rating: {
            fill: 'fa-star',
            empty: 'fa-star-o',
            higherIsBetter: true
        },
        trust: {
            fill: 'fa-check-circle',
            empty: 'fa-check-circle-o',
	        higherIsBetter: true
        },
        clickbait: {
            fill: 'fa-frown-o',
            empty: 'fa-frown-o',
	        higherIsBetter: false
        }
    };

    // region Live search
    $('#search').keyup(function () {
        var value = $(this).val().trim();

	    if (0 == value.length) {
		    delete globalTitle.search;
	    }

	    if (query.search == value) {
	        return;
        }

        query.search = value;
        query.page = 0;
        hasReachedLastResult = false;
	    globalTitle.search = value;

        $('#websites').data('query', query);

        setTimeout(function () {
            if (value == $('#search').val().trim()) {
                fetchWebsites(query, false, function () {
	                pageOps.changePageState(
	                	{
			                title: generateTitle(globalTitle),
			                elements: {
			                	'#search': {type: 'input', value: value},
				                '#websites': {value: $('#websites').html()}
			                }
		                },
		                pageOps.getNewUrl(null, query)
	                );
                });
            }
        }, 200);
    });
    // endregion

    // region Ratings hover effects
    // handle ratings mouseover
    $(document).on('mouseover', '.stars i', function () {
        var type = $(this).parent().data('vote-type');

        for (var i = 0; i <= $(this).index(); i++) {
            $(this).parent().find('i:eq(' + i + ')')
                .removeClass(ratingsConfig[type].empty + ' text-warning')
                .addClass(ratingsConfig[type].fill + ' text-info');
        }
    });

    // handle ratings mouseleave
    $(document).on('mouseleave', '.stars i', function () {
        var defaultRating = $(this).parent().data('rating');
        var type = $(this).parent().data('vote-type');

        for (var i = 0; i <= 4; i++) {
            var classesToAdd = i + 1 <= defaultRating ? ratingsConfig[type].fill + ' text-warning' : ratingsConfig[type].empty;

            $(this).parent().find('i:eq(' + i + ')')
                .removeClass('fa-star-o fa-star text-warning text-info')
                .addClass(classesToAdd);
        }
    });
    // endregion

    // region Rating
    $(document).on('click', '.stars i', function () {
        var id = $(this).parents('.website').data('id');
        var type = $(this).parent().data('vote-type');
        var vote = $(this).index() + 1;
        var container = $(this).parent();
        var currentContent = container.html();

        $.ajax({
            type: 'post',
            url: '/vote/' + id + '/' + type + ':' + vote,
            success: function (data) {
                if (1 == data.code) {
                    container.html(data.message);

                    if (data.newValue) {
                        currentContent = updateRating(type, data.newValue);
                        container.data('rating', data.newValue);
                    }
                } else {
                    container.html('<span class="text-danger">' + data.message + '</span>');
                }

                setTimeout(function () {
                    container.html(currentContent).find('i:eq(4)').trigger('mouseleave');

	                var config = ratingsConfig[type];

                    container
	                    .parents('.list-group-item')
	                    .find('.rating-static')
	                    .removeClass()
	                    .addClass('rating-static')
	                    .addClass(data.newValue == 3
		                    ? ''
		                    : (
			                    (config.higherIsBetter ? data.newValue < 3 : data.newValue > 3)
				                    ? 'text-danger'
				                    : 'text-success'
		                    )
                        )
	                    .text(data.newValue + ' / 5')
                    ;
                }, 2000);
            },
            error: function (err) {
                console.log('error', err);
            }
        })
    });
    // endregion

    // region Sorting
    $('#sort').change(function () {
        var value = $(this).find('option:selected').val();

        if (query.sort == value) {
        	delete globalTitle.sort;
            return ;
        }

        query.sort = value;
        query.page = 0;
        globalTitle.sort = $(this).find('option:selected').text();

        if (query.sort == 0) {
        	delete globalTitle.sort;
        }

        hasReachedLastResult = false;

        fetchWebsites(query);

	    pageOps.changePageState(
		    {
			    title: generateTitle(globalTitle),
			    html: $('#websites').html()
		    },
		    pageOps.getNewUrl(null, query)
	    )
    });
    // endregion

    // region Scroll control
    var fetching = false;

    var isInfiniteScroll = $('body').data('is-infinite-scrolling');
    $(document).scroll(function () {
    	if (!isInfiniteScroll) {
    		return ;
	    }

        var position = $(document).scrollTop();
        var documentHeight = $(document).height();
        var bottomPosition = $(window).height() + position;
        var shouldLoad = documentHeight - bottomPosition <= 200;

        if (false === fetching && shouldLoad && !hasReachedLastResult) {
            var nextPage = ++query.page;
            fetching = nextPage;

            fetchWebsites(query, 1, function () {fetching = false;});
        }
    });
    // endregion

	// region Add website
	$('#add-domain').blur(function () {
		var val = $(this).val().trim();

		if (!val) {
			return;
		}

		var domain = val;
		try {
			domain = getDomain(val);
		} catch (e) {}

		$(this).val(domain);

		$(this).addClass('loading');

		$.ajax({
			url: '/getwebsitemetadata/' + domain,
			success: function(data) {
				var endResult = '';

				if (data.title) {
					endResult += data.title;
				}

				if (data.description) {
					endResult += (data.title ? ".\n" : '') + data.description;
				}

				endResult = endResult.replace("\n", '').replace(/\s\s+/g, ' ');

				if (endResult.toLowerCase().indexOf('news') !== -1 &&
					!$('#add-category option:selected').val()) {
					$('#add-category').val($('#add-category option[data-value="news"]').val());
				}

				$('#add-description').text(endResult);
			},
			error: function (e) {
				$('#add-domain').removeClass('loading');

				if (404 == e.status) {
					alert('Invalid website');
				}
			}
		}).then(function() {
			$('#add-domain').removeClass('loading');
		});
	});

    $('#submit-website').click(function () {
        var data = {
            domain: $('#add-domain').val().trim(),
            description: $('#add-description').val(),
            category: $('#add-category option:selected').val(),
	        geo: $('#add-country option:selected').val()
        };

        if (!data.domain || data.domain.length <= 3) {
            alert('You must enter a domain.');
            return;
        }

        if (!data.category) {
            alert('You must choose a category.');
            return;
        }

        if (!data.geo) {
        	delete data.geo;
        }

	    loader.show();

        $.ajax({
            url: '/submit',
            data: data,
            method: 'POST',
            success: function (response) {
                var classname = 1 == response.code ? 'alert-success' : (0 == response.code ? 'alert-danger' : 'alert-warning');
                $('#add-response').html('<div class="alert ' + classname + '">' + response.message + '</div>');

                if (1 == response.code) {
	                $('.btn-group')
	                    .find('label')
	                    .removeClass('active')
	                    .end()
	                    .find('[type="radio"]')
	                    .prop('checked', false);
                    $('#add-domain').val('');
                    $('#add-description').val('');
                    $('#add-country').val('');
                }

	            loader.hide();
            },
            error: function (err) {
                $('#add-response').html('<div class="alert alert-danger">An error occurred. Please contact the admin.</div>');

	            loader.hide();
            }
        })
    });
    // endregion

	// region Post feedback
	$('#feedback button').click(function () {
		var data = {
			name: $('#fb-name').val().trim(),
			email: $('#fb-email').val().trim(),
			text: $('#fb-text ').val().trim()
		};

		if (!data.name || data.name.length <= 2) {
			alert('You must enter a name.');
			return;
		}

		if (!data.email) {
			alert('Email is required.');
			return;
		}

		if (!data.text || data.text.length <= 2) {
			alert('Text is required.');
			return;
		}

		loader.show();

		$.ajax({
			url: $(this).data('url'),
			data: data,
			method: 'POST',
			success: function (response) {
				if (1 == response.code) {
					$('#feedback .form').html('<div class="alert alert-success">' + response.message + '</div>');
					return;
				}

				alert(response.message);
			},
			error: function (err) {
				alert('An error occurred');
			}
		}).then(function () {
			loader.hide();
		});
	});
	// endregion

	// region On region change
	$('body').on('change', '#region-select', function () {
		var newRegion = $(this).val();

		query.geoId = newRegion;
		query.page = 0;

		if (!newRegion) {
			delete globalTitle.geo;
		} else {
			globalTitle.geo = $(this).find('option:selected').text();
		}

		fetchWebsites(query);

		pageOps.changePageState(
			{
				title: generateTitle(globalTitle),
				html: $('#websites').html()
			},
			pageOps.getNewUrl(null, query)
		)
	});
	// endregion

	//region Subscription
	$('#subscribe-form').on('submit', function (e) {
		e.preventDefault();

		var data = {
			frequency: $('#subscribe-frequency option:selected').val(),
			type: $('#subscribe-type option:selected').val(),
			name: $('#subscribe-name').val(),
			email: $('#subscribe-email').val()
		};

		var url = $(this).attr('action');

		if (!data.email) {
			alert('Email is required!');
			return false;
		}

		$.ajax({
			method: 'POST',
			url: url,
			data: data,
			success: function (data) {
				alert(data.message);

				if (1 == data.code) {
					pageOps.hideSubscribeBar(true);
				}
			},
			error: function (err) {}
		});
	});
	//endregion

    var fetchWebsites = function (query, appendNew, success) {
        var container = $('#websites');
        success = success || function () {};

	    loader.show();

	    return $.ajax({
            method: 'post',
            url: container.data('endpoint'),
            data: query,
            success: function (data) {
	            loader.hide();

                if (data.page == 0 && !data.websites.length) {
                	container.html('<div class="alert alert-warning">Sorry, we can\'t find sites matching your criteria :/</div>');
                } else {
	                container.html(appendNew
		                ? container.html() + buildResults(data.template, data.websites)
		                : buildResults(data.template, data.websites));
                }

	            themeManager.updateCardsBackgrounds();

                hasReachedLastResult = 0 == data.websites.length;

                success();
            },
            error: function (err) {
	            loader.hide();
                hasReachedLastResult = false;
                console.error(err);
            },
        })
    }

    var buildResults = function (template, websites) {
    	if (!websites) {
    		return '';
	    }

    	loader.show();
    	var result = '';

    	$.each(websites, function (key, website) {
    		var site = template.replace(/{{ website.id }}/g, website.id);
		    site = site.replace(/{{ website.name }}/g, website.name);
		    site = site.replace(/{{ website.snapshot }}/g, website.snapshot);
		    site = site.replace(/{{ website.domain }}/g, website.domain);
		    site = site.replace(/{{ website.trust_message }}/g, website.trust_message);
		    site = site.replace(/{{ website.trust_index }}/g, website.trust_index);
		    site = site.replace(/{{ website.avg_rating }}/g, website.avg_rating);
		    site = site.replace(/{{ website.avg_trust_rating }}/g, website.avg_trust_rating);
		    site = site.replace(/{{ website.avg_clickbait_rating }}/g, website.avg_clickbait_rating);

		    var $site = $($(site)[0]);

		    $site.find('.stars').each(function (k, val) {
		        var key = $(val).data('vote-type');
		        var dataKey = key === 'rating' ? 'avg_rating' : 'avg_' + key + '_rating';
		        var config = ratingsConfig[key];

                $(val).html(updateRating(key, website[dataKey]));

			    $(val)
                    .parents('.list-group-item')
                    .find('.rating-static')
                    .removeClass()
                    .addClass('rating-static')
                    .addClass(website[dataKey] == 3
                        ? ''
                        : (
                            (config.higherIsBetter ? website[dataKey] < 3 : website[dataKey] > 3)
                                ? 'text-danger'
                                : 'text-success'
                        )
                    );
            });

		    result += $site.outerHTML();
	    });

	    loader.hide();

    	return result;
    }

    var updateRating = function (type, newValue) {
	    var stars = [];

	    for (var i = 1; i <= 5; i++) {
		    stars.push($('<i></i>')
			    .addClass('fa')
			    .addClass(
			    i <= newValue
				    ? ratingsConfig[type].fill
				    + ' text-warning'
				    : ratingsConfig[type].empty
			    )
			    .outerHTML()
		    );
	    }

	    return stars.join('');
    }
});