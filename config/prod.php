<?php

// configure your app for the production environment

use Symfony\Bridge\Monolog\Logger;

$app['twig.path'] = [__DIR__ . '/../templates'];
$app['monolog.level'] = Logger::NOTICE;
//$app['twig.options'] = array('cache' => __DIR__.'/../var/cache/twig');
