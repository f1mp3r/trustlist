<?php

use Silex\Provider\WebProfilerServiceProvider;
use Symfony\Bridge\Monolog\Logger;

// include the prod configuration
require __DIR__.'/prod.php';

// enable the debug mode
$app['debug'] = true;
$app['monolog.level'] = Logger::DEBUG;

$app->register(new WebProfilerServiceProvider(), array(
    'profiler.cache_dir' => __DIR__.'/../var/cache/profiler',
));
