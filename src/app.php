<?php

use KEIII\SilexSitemap\SitemapServiceProvider;
use Services\Geolocator;
use LinkORB\ConfigLoader\Provider\ConfigurationLoaderProvider;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\RotatingFileHandler;
use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\HttpFoundation\Request;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app->register(new ConfigurationLoaderProvider());
$app['snapshot_dir_public'] = '/images/screenshots/';
$app['snapshot_dir'] = 'web' . $app['snapshot_dir_public'];
$app['asset.counter.js'] = 3;
$app['asset.counter.css'] = 1;
$app['per_page_limit'] = 15;

$app['twig'] = $app->extend('twig', function ($twig, $app) {
	$twig->addGlobal('categories', $categories = $app['db']->fetchAll('SELECT id, name, slug FROM categories ORDER BY `id` ASC'));
	$twig->addGlobal('categoriesInUse', $categories = $app['db']->fetchAll('SELECT c.id, c.name, c.slug FROM categories c INNER JOIN websites w ON w.category_id = c.id GROUP BY c.id ORDER BY c.`id` ASC'));
	$twig->addGlobal('geos', $categories = $app['db']->fetchAll('SELECT id, name FROM geos ORDER BY `id` ASC'));
	$twig->addGlobal('projectName', 'TrustList.co');
	$twig->addGlobal('snapshotDir', $app['snapshot_dir_public']);
	$twig->addGlobal('staticJsCounter', $app['asset.counter.js']);
	$twig->addGlobal('staticCssCounter', $app['asset.counter.css']);

    return $twig;
});

$app['config.loader.env']->load(file_exists('../.env') ? '../.env' : '.env');
$app['config.monolog.dir'] = __DIR__ . '/..' . getenv('LOGS_DIR');
$app['config.geoip.endpoint'] = 'http://ip-api.com/json/%s';

$app->register(new Silex\Provider\DoctrineServiceProvider(), [
    'db.options' => [
        'driver'   => getenv('DB_DRIVER'),
        'dbname' => getenv('DB_NAME'),
        'user' => getenv('DB_USER'),
        'password' => getenv('DB_PASS')
    ],
]);

$app->register(new Silex\Provider\MonologServiceProvider(), [
	'monolog.handler' => (new RotatingFileHandler($app['config.monolog.dir'], 0, Logger::DEBUG))
		->setFormatter(new JsonFormatter('JSON_FORMATTER', 'TL')),
	'monolog.level' => Logger::getLevels()[getenv('LOG_LEVEL')],
]);

$app['geoip'] = function () use ($app) {
	return new Geolocator(new \GuzzleHttp\Client(), $app['config.geoip.endpoint']);
};

$app->register(new SitemapServiceProvider(), [
	'sitemap.domain' => 'https://trustlist.co',
	'sitemap.path' => __DIR__ . '/../web/',
	'sitemap.loc' => 'https://trustlist.co/',
]);

$app['alexa'] = function () {
//	$secretAccessKey = 'OElftM+46/opX8RsUkVi7LQoWyL/zc7kAVh31ux7';
//	$accessKeyId = 'AKIAIOP6LN73F7ZKXZAQ';
};

$app['search'] = function ($app) {
	return function ($queryParams) use ($app) {
		// +x -> DESC X
		// -x -> ASC X
		$allowedSorts = [
			0 => [
				'-avg_rating',
				'-avg_trust_rating',
				'+avg_clickbait_rating',
				'+domain'
			], // Default sorting
			1 => ['+avg_rating', '-domain'], // Lowest rating,
			2 => ['-avg_rating', '-domain'], // Highest rating
			3 => ['+avg_trust_rating', '-domain'], // Least trustworthy
			4 => ['-avg_trust_rating', '-domain'], // Most trustworthy
			5 => ['+avg_clickbait_rating', '-domain'], // Least clickbait-y
			6 => ['-avg_clickbait_rating', '-domain'], // Most clickbait-y
		];
		$trustIndexFormula = 'IF(avg_rating = 0 OR avg_trust_rating = 0 OR avg_clickbait_rating = 0, 0, (avg_rating + avg_trust_rating + (6 - avg_clickbait_rating)) / 3)';

		$query = '
			SELECT
				id,
			    domain,
		        CONCAT("' . $app['snapshot_dir_public'] . '", snapshot) as snapshot,
	            IFNULL(name, domain) as name,
	            category_id,
	            avg_rating,
	            avg_trust_rating,
	            avg_clickbait_rating,
	            TRUNCATE(' . $trustIndexFormula . ', 2) as trust_index,
	            IF(0 = ' . $trustIndexFormula . ', "Unrated", IF(' . $trustIndexFormula . ' >= 4, "You can trust this website", IF(' . $trustIndexFormula . ' >= 2.5, "Not very trustworthy", "Fake news"))) as `trust_message`
            FROM websites
            %s
            ORDER BY %s
            LIMIT ' . ($queryParams->page * $queryParams->limit) . ', ' . $queryParams->limit;
		$where = ['(approved = ?)'];
		$params = [1];
		$order = $allowedSorts[0];

		if ($queryParams->search) {
			$queryParams->search = str_replace('%', '', $queryParams->search);

			$where[] = '(domain LIKE ? OR description LIKE ?)';
			$params[] = '%' . $queryParams->search . '%';
			$params[] = '%' . $queryParams->search . '%';
		}

		if ($queryParams->categoryId) {
			$where[] = '(category_id = ?)';
			$params[] = $queryParams->categoryId;
		}

		if ($queryParams->geoId) {
			$where[] = '(geo_id = ?)';
			$params[] = $queryParams->geoId;
		}

		if ($queryParams->sort && isset($allowedSorts[$queryParams->sort])) {
			$order = $allowedSorts[$queryParams->sort];
		}

		$orderForWhere = [];
		foreach ($order as $value) {
			$sort = $value[0] == '-' ? 'DESC' : 'ASC';
			$orderForWhere[] = '`' . substr($value, 1) . '` ' . $sort;
		}

		$query = sprintf($query, $where ? 'WHERE ' . implode(' AND ', $where) : '', implode(', ', $orderForWhere));

		return $app['db']->fetchAll($query, $params);
	};
};

$app['getQueryParams'] = function ($app) {
	return function (Request $request) use ($app) {
		$isPost = 'post' === strtolower($request->getMethod());

		$queryParams = (object) [
			'search' => $isPost ? $request->request->get('search', null) : $request->get('search', null),
			'categoryId' => $isPost ? $request->request->getInt('categoryId', null) : (int) $request->get('categoryId', 0),
			'geoId' => $isPost ? $request->request->getInt('geoId', null) : (int) $request->get('geoId', 0),
			'sort' => $isPost ? $request->request->getInt('sort', null) : (int) $request->get('sort', 0),
			'page' => $isPost ? $request->request->getInt('page', null) : (int) $request->get('page', 0),
			'limit' => $app['per_page_limit']
		];

		return $queryParams;
	};
};

return $app;
