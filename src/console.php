<?php

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

$console = new Application('TrustList', '0.1');
$console->getDefinition()->addOption(
	new InputOption('--env', '-e', InputOption::VALUE_REQUIRED, 'The Environment name.', 'dev')
);
$console->setDispatcher($app['dispatcher']);

$console
    ->register('sites')
    ->setDescription('List websites')
	->setDefinition(
		[
			new InputOption('filter', 'F', InputOption::VALUE_OPTIONAL, 'Filter: new | all', 'all'),
			new InputOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Limit number of websites'),
			new InputOption('search', 's', InputOption::VALUE_OPTIONAL, 'Search for a website'),
			new InputOption('fields', 'f', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'Properties of websites to show', ['id', 'domain']),
		]
	)
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
    	$where = [1];
    	$limit = $input->getOption('limit') ? 'LIMIT ' . $input->getOption('limit') : '';
    	$search = $input->getOption('search') ? '(domain like "%' . $input->getOption('search') . '%" or description like "%' . $input->getOption('search') . '%")' : '';
    	$fields = $input->getOption('fields');
    	$filter = $input->getOption('filter');
    	$where[] = $search;

    	if (!in_array($filter, ['new', 'all'])) {
    		$output->writeln('Invalid filter ' . $filter);
    		return;
	    }

    	if ('new' == $filter) {
    		$where[] = '(approved = 0)';
	    }

    	$where = implode(' AND ', array_filter($where));
    	$query = 'SELECT ' . implode(', ', $fields) . ' FROM websites WHERE ' . $where . ' ' . $search . ' ORDER BY id ASC ' . $limit;

    	$output->writeln('Query: ' . $query);
        $newWebsites = $app['db']->fetchAll($query);

        $table = (new Table($output))
	        ->setHeaders(array_map('ucfirst', $fields))
	        ->setRows($newWebsites);

        $table->render();
    })
;

$console
	->register('site:approve')
	->addArgument('domain', InputArgument::REQUIRED, 'The domain of the website')
	->setDescription('Approve website and make a screenshot')
	->setCode(function (InputInterface $input, OutputInterface $output) use ($app, $console) {
		$domain = $input->getArgument('domain');
		$website = $app['db']->fetchAll('SELECT id, domain, snapshot FROM websites WHERE approved = 0 AND domain = "' . $domain . '"');

		if (!$website) {
			$output->write('Website ' . $domain . ' either doesn\'t exist or is approved');
			return;
		}
		$website = $website[0];

		$output->writeln('Approving...');
		$app['db']->exec('UPDATE websites SET approved = 1 WHERE id = ' . $website['id']);
		$output->writeln('Approved!');

		if (!$website['snapshot']) {
			$console
				->find('site:snapshot')
				->run(
					new ArrayInput(
						[
							'command' => 'site:snapshot',
							'domain' => $domain
						]
					),
					$output
				);
		}
	})
;

$console
	->register('site:snapshot')
	->setDescription('Make a screenshot of a website')
	->addArgument('domain', InputArgument::REQUIRED, 'The domain of the website')
	->addOption('save', 's', InputOption::VALUE_OPTIONAL, 'Whether to save the screenshot to database', true)
	->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
		$domain = $input->getArgument('domain');
		$save = 'false' === $input->getOption('save') ? false : (bool) $input->getOption('save');
		$website = $app['db']->fetchAll('SELECT id, domain, snapshot FROM websites WHERE domain = "' . $domain . '"');

		if (!$website) {
			$output->write('Website ' . $domain . ' either doesn\'t exist');
			return;
		}

		$website = $website[0];
		$filename = md5(microtime()) . '.png';
		$location = $app['snapshot_dir'] . 'big' . $filename;
		$locationSmall = $app['snapshot_dir'] . $filename;

		$output->writeln('Creating a screenshot...');
		$output->write(shell_exec('/usr/bin/firefox -screenshot ' . $location . ' http://' . $domain . ' --window-size=1920,1080'));
		$output->writeln('Resizing image...');
		$output->write(shell_exec('convert ' . $location . ' -quality 70 -resize 320x180 ' . $locationSmall));
		$output->writeln('Removing big screenshot...');
		$output->write(shell_exec('rm ' . $location));
		$output->writeln('Screenshot: ' . $locationSmall);

		if ($save) {
			if ($website['snapshot']) {
				shell_exec('rm ' . $app['snapshot_dir'] . $website['snapshot']);
			}

			$app['db']->exec('UPDATE websites SET snapshot = "' . $filename . '" WHERE id = ' . $website['id']);
			$output->writeln('Database entry updated');
		}
	})
;

$console
	->register('sites:snapshot')
	->setDescription('Create screenshot for all approved websites without one')
	->addOption('save', 's', InputOption::VALUE_OPTIONAL, 'Whether to save the screenshot to database', true)
	->setCode(function (InputInterface $input, OutputInterface $output) use ($app, $console) {
		$websites = $app['db']->fetchAll('SELECT id, domain FROM websites WHERE approved = 1 AND snapshot IS NULL');

		foreach ($websites as $website) {
			try {
				$output->writeln('Website: ' . $website['domain'] . ' (' . $website['id'] . '):');
				$console
					->find('site:snapshot')
					->run(
						new ArrayInput(
							[
								'command' => 'site:snapshot',
								'domain' => $website['domain'],
								'--save' => $input->getOption('save')
							]
						),
						$output
					);
			} catch (Exception $e) {
				$output->writeln('Site: ' . $website['domain'] . ': ' . $e->getMessage());
			}
		}
	})
;

$console
	->register('sitemap')
	->setDescription('Generate sitemap.xml')
	->setCode(function (InputInterface $input, OutputInterface $output) use ($app, $console) {
		$output->writeln('Starting sitemap.xml generation...');

		$result = file_get_contents('https://trustlist.co/generate-sitemap');
		if ('Ok' === $result) {
			$output->writeln('<info>sitemap.xml generated successfully!</info>');
		} else {
			$output->writeln('<error>' . $result . '</error>');
		}
	})
;


# /usr/bin/firefox -screenshot {location.jpg}  {domain} --window-size=1920,1080
# convert {location.jpg} -resize 320x180 {location320x180.jpg}
# rm {location.jpg}

return $console;
