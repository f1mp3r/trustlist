<?php

namespace Services;

use GuzzleHttp\Client;

class Geolocator
{
	private $endpoint;
	private $client;

	/**
	 * Geolocator constructor.
	 *
	 * @param Client $client
	 * @param string $endpoint
	 */
	public function __construct(Client $client, string $endpoint)
	{
		$this->client = $client;
		$this->endpoint = $endpoint;
	}

	/**
	 * @param string $ip
	 *
	 * @return array
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function getCountry(string $ip)
	{
		$result = json_decode($this->client->get(sprintf($this->endpoint, $ip))->getBody());

		return [
			'country_code' => (isset($result->countryCode) ? $result->countryCode : null),
			'region' => (isset($result->regionName) ? $result->regionName : null),
		];
	}
}