<?php

use KEIII\SilexSitemap\Sitemap;
use KEIII\SilexSitemap\SitemapItem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

$voteTypes = (object) [
    'rating' => 0,
    'trust' => 1,
    'clickbait' => 2
];
$app['debug'] = getenv('DEBUG') ?? false;

#region Homepage
$app->get('/', function (Request $request) use ($app) {
	$queryParams = $app['getQueryParams']($request);

	$websites = $app['search']($queryParams);

	$websites = array_map(function ($site) {
		$site['avg_rating'] = (int) $site['avg_rating'];
		$site['avg_trust_rating'] = (int) $site['avg_trust_rating'];
		$site['avg_clickbait_rating'] = (int) $site['avg_clickbait_rating'];
		return $site;
	}, $websites);

	unset($queryParams->limit);

	return $app['twig']->render('index.html.twig', ['websites' => $websites, 'query' => $queryParams]);
})->bind('homepage');
#endregion

#region Comments
$app->get('/site/{websiteId}/comments', function ($websiteId, Request $request) use ($app) {
	$websiteId = (int) $websiteId;
	$page = (int) $request->get('page', 0);
	$comments = $app['db']->fetchAll('
		SELECT
            id,
            `comment`,
	        CONCAT(SUBSTR(ip, 1, 2), SUBSTR(ip, 15, 2), SUBSTR(ip, -2)) as `uniqid`,
	        parent_id,
	        IFNULL((SELECT CONCAT(SUBSTR(pc.ip, 1, 2), SUBSTR(pc.ip, 15, 2), SUBSTR(pc.ip, -2)) as `uniqid` FROM comments pc WHERE pc.id = c.parent_id), NULL) as parent_unique_id,
	        (SELECT GROUP_CONCAT(p.id SEPARATOR ",") as ids FROM comments p WHERE p.parent_id = c.id GROUP BY p.parent_id) as children,
	        region,
	        country_code,
	        date_added,
	       "' . $app['url_generator']->generate('add-comment', ['websiteId' => $websiteId]) . '" as postUrl
		FROM comments c
		WHERE website_id = ' . $websiteId . '
	    ORDER BY id
	    DESC LIMIT ' . ($page * $app['per_page_limit']) . ', ' . $app['per_page_limit']);

	$template = file_get_contents($app['twig.path'][0] . '/partial/comment.html.twig');

	return new JsonResponse([
		'template' => $template,
		'comments' => $comments,
		'nextPage' => $app['url_generator']->generate('comments', ['websiteId' => $websiteId, 'page' => $page + 1]),
	]);
})->bind('comments');
#endregion

#region Add comment
$app->post('/site/{websiteId}/comments', function ($websiteId, Request $request) use ($app) {
	$comment = trim($request->request->get('comment', null));
	$parentId = trim($request->request->get('parent_id', null)) ?: null;
	$websiteId = (int) $websiteId;
	$ratingId = $request->request->get('rating_id', null);

	if (!$comment) {
		return new JsonResponse(['code' => 0, 'message' => 'Comment is required. (min 4 characters)']);
	}

	if ($comment > 3000) {
		return new JsonResponse(['code' => 0, 'message' => 'Comment too long. (max 3000 characters)']);
	}

	if (!$app['db']->fetchAll('SELECT id FROM websites WHERE id = ' . $websiteId)) {
		return new JsonResponse(['code' => 0, 'message' => 'Website does not exist']);
	}

	$ip = $request->getClientIp();
	$md5ip = md5($ip);

	//	if ($app['db']->fetchAll('SELECT id FROM comments WHERE ip = "' . $md5ip . '" and website_id = ' . $websiteId)) {
	//		return new JsonResponse(['code' => 0, 'message' => 'You can only post one comment per website']);
	//	}

	$geo = $app['geoip']->getCountry($ip);

	$app['db']->insert('comments', [
		'comment' => $comment,
		'website_id' => $websiteId,
		'rating_id' => $ratingId,
		'ip' => $md5ip,
		'country_code' => strtolower($geo['country_code']),
		'parent_id' => $parentId,
		'region' => $geo['region'],
		'date_added' => date('Y-m-d H:i:s')
	]);

	return new JsonResponse(['code' => 1, 'message' => 'Comment posted!']);
})->bind('add-comment');
#endregion

#region Site details
$app->get('/site/{domain}', function ($domain, Request $request) use ($app) {
	$where = [
		'w.approved = 1',
		'w.domain = "' . $domain . '"'
	];

	$website = $app['db']->fetchAll('SELECT w.*, g.name as `geo` FROM websites w LEFT JOIN geos g ON g.id = w.geo_id WHERE ' . implode(' AND ', $where));

	if (!$website) {
		throw new RouteNotFoundException('Website not found');
	}

	$website = $website[0];

	$commentsQuery = '
		SELECT
            id,
            `comment`,
	        CONCAT(SUBSTR(ip, 1, 2), SUBSTR(ip, 15, 2), SUBSTR(ip, -2)) as `uniqid`,
	        parent_id,
	        IFNULL((SELECT CONCAT(SUBSTR(pc.ip, 1, 2), SUBSTR(pc.ip, 15, 2), SUBSTR(pc.ip, -2)) as `uniqid` FROM comments pc WHERE pc.id = c.parent_id), NULL) as parent_unique_id,
	        (SELECT GROUP_CONCAT(p.id SEPARATOR ",") as ids FROM comments p WHERE p.parent_id = c.id GROUP BY p.parent_id) as children,
	        region,
	        country_code,
	        date_added
		FROM comments c
		WHERE website_id = ' . $website['id'] . '
	    ORDER BY id DESC
        LIMIT 0, ' . $app['per_page_limit'];

	$comments = $app['db']->fetchAll($commentsQuery);
	$votes = $app['db']->fetchAll('SELECT DATE(date_created) as `date`, AVG(rating) as `rating`, type FROM ratings WHERE website_id = ' . $website['id'] . ' AND date_created >= DATE_SUB(NOW(), INTERVAL 1 MONTH) GROUP BY type, DATE(date_created)');
	$avgVotes = $app['db']->fetchAll('SELECT type, COUNT(id) as `count` FROM ratings WHERE website_id = ' . $website['id'] . ' AND date_created >= DATE_SUB(NOW(), INTERVAL 1 MONTH) GROUP BY type');
	$voters = array_combine(array_column($avgVotes, 'type'), array_column($avgVotes, 'count')) ?: [0 => 0, 1 => 0, 2 => 0];
	$ratings = [];

	foreach ($votes as $vote) {
		$ratings[$vote['type']][$vote['date']] = $vote['rating'];
	}

	if ($website['snapshot']) {
		$website['snapshot'] = $app['snapshot_dir_public'] . $website['snapshot'];
	}

	foreach ($ratings as $type => $dates) {
		$lastDateRating = null;
		$checkDate = new DateTime(date('Y-m-d', strtotime('-30 days')));
		$today = new DateTime();

		while ($checkDate <= $today) {
			if (!isset($dates[$checkDate->format('Y-m-d')])) {
				$ratings[$type][$checkDate->format('Y-m-d')] = $lastDateRating;
			} else {
				$lastDateRating = $ratings[$type][$checkDate->format('Y-m-d')];
			}

			$checkDate = $checkDate->add(new DateInterval('P1D'));
		}

		uksort($ratings[$type], function ($firstDate, $secondDate) {
			return (new DateTime($firstDate)) <=> (new DateTime($secondDate));
		});
	}

	$isAjax = $request->isXmlHttpRequest();
	$userIp = md5($request->getClientIp());
	$userHasCommented = false;//$app['db']->fetchAll('SELECT id FROM comments WHERE website_id = ' . $website['id'] . ' AND ip = "' . $userIp. '"') ? true : false;

	$html = $app['twig']->render(($isAjax ? 'partial/' : '' ) . 'website.html.twig', [
		'userHasCommented' => $userHasCommented,
		'commentsCount' => $app['db']->fetchColumn('SELECT COUNT(id) FROM comments WHERE website_id = ' . $website['id']),
		'website' => $website,
		'ratings' => $ratings,
		'isAjax' => $isAjax,
		'voters' => $voters,
		'comments' => $comments
	]);

	if (!$request->isXmlHttpRequest()) {
		return $html;
	}

	return new JsonResponse([
		'html' => $html,
		'title' => $website['domain'] . ' statistics',
		'id' => $website['id'],
	]);
})->bind('site')->assert('domain', '.+');
#endregion

#region Add new site
$app->get('/submit', function (Request $request) use ($app) {
    $data = [
        'domain' => trim($request->request->get('domain', '')),
        'category' => $request->request->getInt('category', 0),
        'description' => $request->request->get('description', null),
	    'geo' => $request->request->get('geo', null)
    ];

    $data['domain'] = str_replace(['https://', 'http://', 'www.'], ['', '', ''], $data['domain']);

    if (strlen($data['domain']) <= 3) {
        return new JsonResponse(['code' => 0, 'message' => 'Invalid domain']);
    }

    if ($id = $app['db']->fetchAll('SELECT id FROM websites WHERE domain = ?', [$data['domain']])[0]['id']) {
    	$app['db']->exec('UPDATE websites SET add_requests_count = add_requests_count + 1 where id = ' . $id);
        return new JsonResponse(['code' => 0, 'message' => 'Website is already added']);
    }

    if (!$data['category'] || !$app['db']->fetchAll('SELECT id FROM categories WHERE id = ?', [$data['category']])) {
        return new JsonResponse(['code' => 0, 'message' => 'Invalid category']);
    }

	if ($data['geo'] && !$app['db']->fetchAll('SELECT id FROM geos WHERE id = ?', [$data['geo']])) {
		return new JsonResponse(['code' => 0, 'message' => 'Invalid country / region']);
	}

    if ($data['description'] && strlen($data['description']) > 500) {
        return new JsonResponse(['code' => 0, 'message' => 'Description is too long (500 chars max)']);
    }

	if (checkdnsrr('http://' . $data['domain'], 'ANY')) {
        return new JsonResponse(['code' => 0, 'message' => 'Website does not exist.']);
	}

    $insert = $app['db']->executeUpdate(
        'INSERT INTO websites (`domain`, `description`, `category_id`, `adder_ip`, `date_created`, `geo_id`) VALUES (?, ?, ?, ?, NOW(), ?)',
        [
            $data['domain'],
            $data['description'],
            $data['category'],
            $request->getClientIp(),
            $data['geo'],
        ]
    );

    try {
	    shell_exec('cd ../../mail2im.com && php artisan telegram:send "New website submitted"');
    } catch (Exception $e) {
    }

    return new JsonResponse(['code' => 1, 'message' => $data['domain'] . ' was added and will show up after approval!']);
})->method('POST');
#endregion

#region Live search
$app->get('/search', function (Request $request) use ($app) {
	$queryParams = $app['getQueryParams']($request);

    $websites = $app['search']($queryParams);

    $template = file_get_contents($app['twig.path'][0] . '/partial/website-single.html.twig');

    return new JsonResponse([
    	'template' => $template,
	    'websites' => $websites,
	    'page' => $queryParams->page
    ]);
})->bind('search')->method('POST');
#endregion

#region Vote
$app->get('/vote/{id}/{type}:{vote}', function (Request $request, $id, $type, $vote) use ($app, $voteTypes) {
    if ($vote < 1 || $vote > 5 || !isset($voteTypes->{$type})) {
        return new JsonResponse(['code' => 0, 'message' => 'Invalid vote']);
    }
    $typeId = $voteTypes->{$type};

    if (!($site = $app['db']->fetchAll('SELECT domain FROM websites WHERE id = ?', [$id]))) {
        return new JsonResponse(['code' => 0, 'message' => 'Invalid website']);
    }

    $ip = $request->getClientIp();
    $regionData = $app['geoip']->getCountry($ip);
    $md5ip = md5($ip);
	/**
	 * @var $db \Doctrine\DBAL\Connection
	 */
    $db = $app['db'];

    // Check if user with this IP has voted for the last 7 days
    if ($db->fetchAll('SELECT id FROM ratings WHERE website_id = ? AND ip = ? AND type = ? AND date_created >= DATE_SUB(NOW(), INTERVAL 7 DAY)', [$id, $md5ip, $typeId])) {
        return new JsonResponse([
        	'code' => 0,
	        'message' => 'Already voted for ' . $site['domain'] . ' in the past 7 days',
	        'newValue' => (int) $db->fetchColumn('SELECT AVG(rating) FROM ratings WHERE website_id = ? AND type = ?', [$id, $typeId])
        ]);
    }

    $db->executeUpdate(
        'INSERT INTO ratings (`website_id`, `ip`, `agent`, `type`, `rating`, `date_created`, `region`, `country_code`) VALUES (?, ?, ?, ?, ?, NOW(), ?, ?)',
        [
            $id,
	        $md5ip,
            $request->headers->get('User-Agent'),
            $typeId,
            $vote,
	        $regionData['region'],
	        $regionData['country_code'],
        ]
    );

	$app['db']->executeUpdate('
        UPDATE websites w
        SET
            avg_rating = IFNULL((SELECT AVG(rating) FROM ratings WHERE website_id = w.id AND type = 0), 0),
            avg_trust_rating = IFNULL((SELECT AVG(rating) FROM ratings WHERE website_id = w.id AND type = 1), 0),
            avg_clickbait_rating = IFNULL((SELECT AVG(rating) FROM ratings WHERE website_id = w.id AND type = 2), 0)
    ');

    $averageCurrentValue = (int) $db->fetchColumn('SELECT AVG(rating) FROM ratings WHERE website_id = ? AND type = ?', [$id, $typeId]);

    return new JsonResponse(['code' => 1, 'message' => 'Voted!', 'newValue' => $averageCurrentValue]);
})->method('POST');
#endregion

#region Log outgoing clicks
$app->get('/log/{id}', function ($id) use ($app) {
    if (0 == $id) {
        return '';
    }

    $app['db']->executeUpdate('UPDATE websites SET clicks = clicks + 1 WHERE id = ?', [$id]);

    return '';
})->method('POST')->convert('id', function ($id) { return (int) $id; });
#endregion

#region Aggregate
$app->get('/aggregate', function () use ($app) {
	$app['db']->executeUpdate('
        UPDATE websites w
        SET
            avg_rating = (SELECT AVG(rating) FROM ratings WHERE website_id = w.id AND type = 0),
            avg_trust_rating = (SELECT AVG(rating) FROM ratings WHERE website_id = w.id AND type = 1),
            avg_clickbait_rating = (SELECT AVG(rating) FROM ratings WHERE website_id = w.id AND type = 2)
    ');

	return 'Updated';
});

$app->get('/populate', function () use ($app) {
	$sites = array_column($app['db']->fetchAll('SELECT id FROM websites'), 'id');
	$agents = [
		'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7',
		'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36',
		'Mozilla/5.0 (iPad; CPU OS 9_3_2 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13F69 Safari/601.1/',
		'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36',
		'Mozilla/5.0 (Windows NT 6.1; Win64; x64; Trident/7.0; rv:11.0) like Gecko',
		'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.1.2 Safari/603.3.8',
		'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.1.2 Safari/603.3.8',
		'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10240',
		'Mozilla/5.0 (Windows NT 6.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586',
		'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0'
	];
	set_time_limit(0);

	for ($i = 0; $i <= 300; $i++) {
		$websiteId = $sites[array_rand($sites)];
		$ip = mt_rand(1, 255) . '.' . mt_rand(1, 255) . '.' . mt_rand(1, 255) . '.' . mt_rand(1, 255);
		$agent = $agents[array_rand($agents)];
		$rating = mt_rand(1, 5);
		$type = mt_rand(0, 2);
		$dateCreated = (new DateTime('@' . strtotime('-' . mt_rand(0, 60) . ' days')))->format('Y-m-d H:i:s');

		$app['db']->exec('INSERT INTO ratings (website_id, ip, agent, rating, date_created, type) VALUES (' . $websiteId . ', "' . $ip . '", "' . $agent . '", ' . $rating . ', "' . $dateCreated . '", ' . $type . ')');
	}

	return 'Inserted';
});
#endregion

#region Category
$app->get('/c/{slug}', function ($slug, Request $request) use ($app) {
	$categories = $app['db']->fetchAll('SELECT id, name, slug FROM categories ORDER BY `name` ASC');
	$currentCategory = null;
	$found = false;

	foreach ($categories as $cat) {
		if ($cat['slug'] == $slug) {
			$found = $cat['id'];
			$where[] = 'category_id = ' . $found;
			$currentCategory = $cat;
			break;
		}
	}

	if (!$found) {
		throw new RouteNotFoundException('No such category');
	}

	$request->query->set('categoryId', $found);

	$queryParams = $app['getQueryParams']($request);

	$websites = $app['search']($queryParams);
	$websites = array_map(function ($site) {
		$site['avg_rating'] = (int) $site['avg_rating'];
		$site['avg_trust_rating'] = (int) $site['avg_trust_rating'];
		$site['avg_clickbait_rating'] = (int) $site['avg_clickbait_rating'];
		return $site;
	}, $websites);

	unset($queryParams->limit);

	return $app['twig']->render('index.html.twig', [
		'categories' => $categories,
		'websites' => $websites,
		'selectedCategory' => $currentCategory,
		'selectedCategoryId' => $found,
		'query' => $queryParams
	]);
})->bind('category')->value('slug', null);
#endregion

#region Subscriptions
$app->post('/subscribe', function (Request $request) use ($app) {
	$frequency = $request->request->getInt('frequency', false);
	$category = $request->request->getInt('category', 0);
	$name = $request->request->getAlnum('name', false);
	$email = $request->request->get('email', false);

	if (false === $frequency || ($frequency !== 1 && $frequency !== 0)) {
		return $app->json(['code' => 0, 'message' => 'Frequency of emails is required']);
	}

	if ($category !== 0 && !$app['db']->fetchAll('SELECT COUNT(*) FROM categories WHERE id = ?', [$category])) {
		return $app->json(['code' => 0, 'message' => 'Invalid category']);
	}

	if (!$email || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
		return $app->json(['code' => 0, 'message' => 'Email is required']);
	}

	if ($app['db']->fetchAll('SELECT * from subscribers WHERE email = ?', [$email])) {
		return $app->json(['code' => 0, 'message' => 'Email is already subscribed']);
	}

	$app['db']->insert('subscribers', [
		'frequency' => $frequency,
		'category' => $category,
		'name' => $name,
		'email' => $email,
		'last_email_sent_date' => null,
		'verification_hash' => md5(microtime(true)) . md5($email),
		'date_added' => date('Y-m-d H:i:s')
	]);

	try {
		shell_exec('cd ../../mail2im.com && php artisan telegram:send "New subscription"');
	} catch (Exception $e) {
	}

	#todo: send email for confirmation

	return $app->json(['code' => 1, 'message' => 'You have subscribed successfully!']);
})->bind('subscribe');
#endregion

#region Metadata fetch
$app->get('/getwebsitemetadata/{domain}', function ($domain) use ($app) {
	$websiteContents = file_get_contents('http://' . $domain);
//	return $websiteContents;

	if (!$websiteContents) {
		return $app->json(['error' => 1]);
	}

	preg_match("/<title>(.*)<\/title>/siU", $websiteContents, $titleMatches);
	$title = preg_replace('/\s+/', ' ', $titleMatches[1]);
	$title = trim($title);

	$doc = new DOMDocument();
	@$doc->loadHTML($websiteContents);

	$metas = $doc->getElementsByTagName('meta');
	$description = null;

	for ($i = 0; $i < $metas->length; $i++)
	{
		$meta = $metas->item($i);

		if($meta->getAttribute('name') == 'description') {
			$description = $meta->getAttribute('content');
		}
	}

	return $app->json(['title' => $title, 'description' => $description]);
});
#endregion

#region Post feedback
$app->get('/feedback', function (Request $request) use ($app) {
	$data = [
		'name' => trim($request->request->get('name', null)),
		'email' => trim($request->request->get('email', null)),
		'text' => trim($request->request->get('text', null))
	];
	$ip = $request->getClientIp();

	$insert = $app['db']->executeUpdate(
		'INSERT INTO feedback (`name`, `email`, `text`, `ip`, `date_posted`) VALUES (?, ?, ?, ?, NOW())',
		[
			$data['name'],
			$data['email'],
			$data['text'],
			$ip
		]
	);

	try {
		shell_exec('cd ../../mail2im.com && php artisan telegram:send "New feedback"');
	} catch (Exception $e) {
	}

	return new JsonResponse(['code' => 1, 'message' => 'Thanks, your feedback is appreciated!']);
})->bind('feedback')->method('POST');
#endregion

#region Generate sitemap
$app->get('/generate-sitemap', function () use ($app) {
	/** @var Sitemap $sitemap */
	$sitemap = $app['sitemap'];
	$addLink = function ($url) use ($sitemap) {
		$item = (new SitemapItem())
			->setLoc($url)
			->setPriority(1.0)
			->setChangefreq('daily')
			->setLastmod(new \DateTime())
		;
		$sitemap->addItem($item);
	};

	// Homepage
	$addLink('/');

	#region Categories
	$categories = $app['db']->fetchAll('SELECT id, name, slug FROM categories ORDER BY `name` ASC');

	foreach ($categories as $category) {
		$addLink($app['url_generator']->generate('category', ['slug' => $category['slug']]));
	}
	#endregion

	#region Websites
	$websites = $app['db']->fetchAll('SELECT `domain` FROM websites WHERE approved = 1');

	foreach ($websites as $website) {
		$addLink($app['url_generator']->generate('site', ['domain' => $website['domain']]));
	}
	#endregion

	#region Combinations of category, sorting and countries
	$regions = array_column($app['db']->fetchAll('SELECT id FROM geos'), 'id');
	$sorts = range(0, 6);
	$categoryIds = array_column($categories, 'id');

	foreach ($categoryIds as $categoryId) {
		foreach ($regions as $region) {
			foreach ($sorts as $sort) {
				$addLink($app['url_generator']->generate(
					'homepage',
					[
						'categoryId' => $categoryId,
						'geoId' => $region,
						'sort' => $sort
					]
				));
			}
		}
	}

	$sitemap->create();
	return response('Ok');
});

$app->error(function (Exception $e, Request $request, $code) use ($app) {
	$app['monolog']->error($e);

	if ($request->isXmlHttpRequest()) {
		return new JsonResponse(['code' => 0, 'message' => $e->getMessage()]);
	}

	if ($app['debug']) {
		dump($e);
		return;
    }

	$categories = $app['db']->fetchAll('SELECT id, name, slug FROM categories ORDER BY `name` ASC');

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = [
        'errors/' . $code . '.html.twig',
        'errors/' . substr($code, 0, 2) . 'x.html.twig',
        'errors/' . substr($code, 0, 1) . 'xx.html.twig',
        'errors/default.html.twig',
    ];

    return new Response($app['twig']->resolveTemplate($templates)->render([
    	'code' => $code,
	    'error' => $e,
	    'categories' => $categories
    ]), $code);
});
